package listaSimplesmenteEncadeada;

public class testeLSE {

	public static void main(String[] args) {
		executa(new LSE());
	}

	private static void executa(ILSE lista) {
		lista.inserir(7);
		lista.inserir(9);
		lista.inserir(3);
		lista.inserir(0);
		lista.inserir(4);
		System.out.println("Lista: ");
		lista.imprimir();

		No no1 = lista.buscar(7);
		if (no1 != null) {
			System.out.println("Tem q dar 7");
			System.out.println(no1.valor);
		}
		No no2 = lista.buscar(4);
		if (no2 != null) {
			System.out.println("Tem q dar 4");
			System.out.println(no2.valor);
		}
		No no3 = lista.buscar(3);
		if (no3 != null) {
			System.out.println("Tem q dar 3");
			System.out.println(no3.valor);
		}
		No no4 = lista.buscar(1);
		if (no4 != null) {
			System.out.println("Tem q dar erro");
			System.out.println(no4.valor);
		}
		System.out.println("ALTERAR 7 POR 17");
		lista.alterar(7, 17);
		lista.imprimir();

		System.out.println("ALTERAR 4 POR 24");
		lista.alterar(4, 24);
		lista.imprimir();

		System.out.println("ALTERAR 3 POR 53");
		lista.alterar(3, 53);
		lista.imprimir();

		System.out.println("ALTERAR 15 POR 45");
		lista.alterar(15, 45);
		lista.imprimir();

		lista.remover(17);
		System.out.println("Lista sem 17(primeiro)");
		lista.imprimir();

		lista.remover(53);
		System.out.println("Lista sem 53");
		lista.imprimir();

		lista.remover(24);
		System.out.println("Lista sem 24");
		lista.imprimir();

		lista.remover(55);
		System.out.println("Lista sem 55(n existe), continuar mesmo do anterior");
		lista.imprimir();

	}

}
