package listaSimplesmenteEncadeada;

public interface ILSE {

	
	public void inserir(int valor);
	public void remover(int chave);
	public No buscar(int chave);
	public void alterar(int chave, int novoValor);
	public void imprimir();



}
