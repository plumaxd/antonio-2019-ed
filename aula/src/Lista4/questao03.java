package Lista4;

import java.util.Scanner;

public class questao03 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe o X: ");
		int x = sc.nextInt();
		System.out.println("Informe o Y: ");
		int y = sc.nextInt();
		System.out.println(soma(x, y));
	}

	public static int soma(int x, int y) {
		if (x == 0)
			return y;
		else if (y == 0)
			return x;
		
		if(x > 0 && y < 0) {
			return soma(predecessor(x), sucessor(y));
		}

		return soma(sucessor(x), predecessor(y));
	}
	public static int predecessor(int a) {
		return --a;
	}
	public static int sucessor(int b) {
		return ++b;
	}
	
}
