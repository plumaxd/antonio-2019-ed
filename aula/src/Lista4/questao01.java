package Lista4;

import java.util.Scanner;

public class questao01 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Informe a base: ");
		int base = sc.nextInt();
		System.out.println("Informe a potencia: ");
		int n = sc.nextInt();
		System.out.println(potencia(base, n));

	}

	private static int potencia(int base, int n) {
		if (n == 0) {
			return 1;
		} else {
			return base * potencia(base, n - 1);
		}
	}

}
