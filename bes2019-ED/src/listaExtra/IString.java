package listaExtra;

public interface IString {
	int length();

	char charAt(int posicao);

	boolean equals(String valor);

	boolean startsWith(String valor);

	boolean endWith(String valor);

	int indexOf(char letra);

	int lastIndexOf(char letra);

	String substring(int inicio, int quantidadeDeCaracteres);

	String replace(char letraASerTrocada, char letraATrocar);

	String concat(String valor);

	void imprime();

}
